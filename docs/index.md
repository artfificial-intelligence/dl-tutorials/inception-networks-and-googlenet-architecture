# Inception 네트워크와 GoogleNet 아키텍처 <sup>[1](#footnote_1)</sup>

## 목차

1. [개요](./inception-networks-and-googlenet-architecture.md#intro)
1. [Inception 네트워크란?](./inception-networks-and-googlenet-architecture.md#sec_02)
1. [Inception 네트워크 작동 방식](./inception-networks-and-googlenet-architecture.md#sec_03)
1. [GoogleNet 아키텍처란?](./inception-networks-and-googlenet-architecture.md#sec_04)
1. [Inception 네트워크와 GoogleNet 어플리케이션](./inception-networks-and-googlenet-architecture.md#sec_05)
1. [요약](./inception-networks-and-googlenet-architecture.md#summary)

<a name="footnote_1">1</a>: [DL Tutorial 9 — Inception Networks and GoogleNet Architecture](https://code.likeagirl.io/dl-tutorial-9-inception-networks-and-googlenet-architecture-8f29f2749636?sk=aa9467bcadb958b4ba630cb98c716291)를 편역하였습니다.
